# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT
---
device_type: fvp
job_name: test

timeouts:
  connection:
    minutes: 10
  job:
    minutes: 80
  actions:
    auto-login-action:
      minutes: 5
    fvp-deploy:
      minutes: 20
    download-retry:
      minutes: 20
    http-download:
      minutes: 20

metadata:
  source: ac
  path: .gitlab/lava/corstone1000-fvp/sanity_job.yml.j2
  gitlab-job-url: bd

priority: medium
visibility:
  group:
    - cassini

context:
  boot_character_delay: 250
  test_character_delay: 250


actions:
- deploy:
    to: fvp
    images:
      bl1:
        url: https://gitlab.com/Linaro/cassini/meta-cassini/-/jobs/4914039048/artifacts/raw/work/build/tmp/deploy/images/corstone1000-fvp/bl1.bin
      cs1000:
        url: https://gitlab.com/Linaro/cassini/meta-cassini/-/jobs/4914039048/artifacts/raw/work/build/tmp/deploy/images/corstone1000-fvp/corstone1000-image-corstone1000-fvp-20230821150401.rootfs.wic
      extsys:
        url: https://gitlab.com/Linaro/cassini/meta-cassini/-/jobs/4914039048/artifacts/raw/work/build/tmp/deploy/images/corstone1000-fvp/es_flashfw.bin
      mmc0:
        url: https://gitlab.com/Linaro/cassini/meta-cassini/-/jobs/4914039048/artifacts/raw/work/build/tmp/deploy/images/corstone1000-fvp/cassini-image-base-corstone1000-fvp-20230821150401.rootfs.wic.pad.gz
        compression: gz

- boot:
    method: fvp
    docker:
      name: "registry.gitlab.com/linaro/cassini/meta-cassini/cs1k-fvp-test-image:main"
    image: /opt/models/Linux64_GCC-9.3/FVP_Corstone-1000
    version_string: 'Fast Models [^\\n]+'
    timeout:
      minutes: 15
    console_string: 'host_terminal_0: Listening for serial connection on port (?P<PORT>\d+)'
    feedbacks:
      - '(?P<NAME>host_terminal_1): Listening for serial connection on port (?P<PORT>\d+)'
      - '(?P<NAME>secenc_terminal): Listening for serial connection on port (?P<PORT>\d+)'
      - '(?P<NAME>extsys_terminal): Listening for serial connection on port (?P<PORT>\d+)'
    arguments:
      - "-C se.trustedBootROMloader.fname={BL1}"
      - "-C board.xnvm_size=64"
      - "-C se.trustedSRAM_config=6"
      - "-C se.BootROM_config=3"
      - "-C board.hostbridge.interfaceName=tap0"
      - "-C board.smsc_91c111.enabled=1"
      - "-C board.hostbridge.userNetworking=true"
      - "-C board.hostbridge.userNetPorts=5555=5555,8080=80,8022=22"
      - "-C board.se_flash_size=8192"
      - "-C diagnostics=4"
      - "-C disable_visualisation=true"
      - "-C se.nvm.update_raw_image=0"
      - "-C se.cryptocell.USER_OTP_FILTERING_DISABLE=1"
      - "-C extsys_harness0.extsys_flashloader.fname={EXTSYS}"
      - "-C board.msd_mmc.card_type=SDHC"
      - "-C board.msd_mmc.p_fast_access=0"
      - "-C board.msd_mmc.diagnostics=2"
      - "-C board.msd_mmc.p_max_block_count=0xFFFF"
      - "-C board.msd_config.pl180_fifo_depth=16"
      - "--data board.flash0={CS1000}@0x68000000"
    auto_login:
      login_prompt: 'corstone1000-fvp login:.*'
      username: root
    prompts:
      - 'New password: '
      - 'Re-enter new password: '
      - '@corstone1000-fvp:.+\$ '
      - '@corstone1000-fvp:.+# '
